//
//  ViewController.swift
//  MyWebViewExample
//
//  Created by Administrador on 19/02/16.
//  Copyright © 2016 ITESM. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var myWebView: UIWebView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        myWebView.delegate = self
        
    /* URL in Web View
        let myURL = NSURL(string: "http://www.swiftdeveloperblog.com")
        let myURLRequest:NSURLRequest = NSURLRequest(URL: myURL!)
        myWebView.loadRequest(myURLRequest)
    */


    /*HTML String
        let htmlString: String = "<br/><b style='color:red'>Bold string</b>"
        myWebView.loadHTMLString(htmlString, baseURL: nil)
      */
        
        
        let myProjectBundle:NSBundle = NSBundle.mainBundle()
        
        let filePath:String = myProjectBundle.pathForResource("my-html-file", ofType: "html")!
        
        let myURL = NSURL(string: filePath)
        let myURLRequest:NSURLRequest = NSURLRequest(URL: myURL!)
        
        myWebView.loadRequest(myURLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webViewDidStartLoad(webView: UIWebView){
       myActivityIndicator.startAnimating()
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        myActivityIndicator.stopAnimating()
        
    }
    
    @IBAction func refreshButtonTapped(sender: AnyObject) {
        myWebView.reload()
    }
    
    
}

